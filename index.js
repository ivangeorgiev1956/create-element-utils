export function createListElement(data = [], ordered = false) {
    const listElement = ordered ? document.createElement("ol") : document.createElement("ul");

    data.forEach(function (value) {
        const listItem = document.createElement("li");
        listItem.innerHTML = value;

        listElement.appendChild(listItem);
    });

    return listElement;
}

export function prependListItems(data = [], listElement) {
    data.forEach(function (value) {
        const listItem = document.createElement("li");
        listItem.innerHTML = value;

        listElement.prepend(listItem);
    });
}

export function createForm(inputs = [], onSubmit) {
    const formElement = document.createElement("form");

    inputs.forEach(function (input) {
        if (input.type === "text" || input.type === "number" || input.type === "password") {
            if (input.name) {
                const inputElement = document.createElement("input");
                inputElement.setAttribute("type", input.type);
                inputElement.setAttribute("name", input.name);

                formElement.appendChild(inputElement);
            } else {
                console.warn(`Input type ${input.type} with no name.`);
            }
        } else {
            console.warn(`Input type ${input.type} not supported.`);
        }
    });

    if (onSubmit) {
        const submitInputElement = document.createElement("input");
        submitInputElement.setAttribute("type", "submit");

        formElement.appendChild(submitInputElement);

        formElement.addEventListener("submit", function (event) {
            const values = [];

            event.preventDefault();

            this.childNodes.forEach(function (childNode) {
                if (childNode.name) {
                    values.push({ name: childNode.name, value: childNode.value });
                }
            });

            onSubmit(values);
        });
    }

    return formElement;
}